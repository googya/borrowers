import DS from 'ember-data';

export default DS.Model.extend({
  firstName: DS.attr('string'),
  lastName: DS.attr('string'),
  email: DS.attr('string'),
  twitter: DS.attr('string'),
  totalArticle: DS.attr('number'),
  articles: DS.hasMany('article'),
  fullName: function(){
    return this.get('lastName') + ' & ' + this.get('firstName')
  }.property('lastName', 'firstName')
});
